# SoC Makefile to create bitstream
# (c) Dorfell Parra  <dlparrap@unal.edu.co>


VIVADO_BASE = /usr/local/Xilinx/Vivado/2018.2
VIVADO = $(VIVADO_BASE)/bin/vivado
XVLOG  = $(VIVADO_BASE)/bin/xvlog
XVHDL  = $(VIVADO_BASE)/bin/xvhdl
XELAB  = $(VIVADO_BASE)/bin/xelab
XSIM   = $(VIVADO_BASE)/bin/xsim
GLBL   = $(VIVADO_BASE)/data/verilog/src/glbl.v

export VIVADO


help:
	@echo "Simulate  SoC:       "
	@echo "  make sim_system    "
	@echo "                     "
	@echo "Create SoC bitstream:"
	@echo "  make bitstream     "
	@echo "                     "
	@echo "Program FPGA:        "
	@echo "  make prog          "
	@echo "                     "


sim_system:
	cd hardware/xsim/ && ./tb_top_system.sh
	$(XSIM) -gui hardware/xsim/tb_top_system.wdb


bitstream:
	rm -f $@.log
	$(VIVADO) -nojournal -log $@.log -mode batch -source hardware/top_system.tcl
	rm -rf .Xil *.log *.jou *.xml *.html


prog:
	@echo "Using the Digilent JTAG Config utility"
	@echo "                     "
	djtgcfg enum
	@echo "                     "
	djtgcfg init -d ZYBO-Z7
	@echo "                     "
	djtgcfg prog -d ZYBO-Z7 --index 1 --file top_system.bit
	@echo "                     "


clean:
	rm -rf .Xil/   
	rm -rf top_system.bit synth_system.vhd 
	rm -rf .Xil *.log *.jou *.xml *.html
	rm -rf hardware/*.log hardware/*.jou  hardware/*.Xil/
	./hardware/xsim/tb_top_system.sh -reset_run

