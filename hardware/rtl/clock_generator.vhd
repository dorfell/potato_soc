-- SoC  constraints for Zybo Z7
-- (c) Dorfell Parra  <dlparrap@unal.edu.co>

library ieee;
use ieee.std_logic_1164.ALL;

library unisim;
use unisim.vcomponents.all;


entity clock_generator is
  generic(
--    -- Clk_in 100 [MHz], system_clk = 50 [MHz], timer_clk = 10 [MHz]
--    CLKIN_PERIOD   :    real := 8.0; --Input clock period in ns to ps resolution (i.e.  8.0 ns is 125 MHz).
--    CLKOUT0_DIVIDE : natural :=  16; -- CLKIN = 100 MHz, CLKOUT = 50 [MHz]
--    CLKOUT1_DIVIDE : natural :=  80  -- CLKIN = 100 MHz, CLKOUT = 10 [MHz]
    -- Clk_in 125 [MHz], system_clk = 50 [MHz], timer_clk = 10 [MHz]
    CLKIN_PERIOD   :    real := 8.0; --Input clock period in ns to ps resolution (i.e.  8.0 ns is 125 MHz).
    CLKOUT0_DIVIDE : natural :=  20; -- CLKIN = 100 MHz, CLKOUT = 50 [MHz]
    CLKOUT1_DIVIDE : natural := 100  -- CLKIN = 100 MHz, CLKOUT = 10 [MHz]
  );
  port(
    clk        : in std_logic;
    reset_n    : in std_logic;
    system_clk : out std_logic;
    timer_clk  : out std_logic;
    locked     : out std_logic
  );
end clock_generator;

architecture Behavioral of clock_generator is

signal sig_clkfb : std_logic := '0';
signal sig_rst   : std_logic := '0';

begin

  --PLLE2_BASE:BasePhaseLockedLoop(PLL)
  --7Series
  --XilinxHDLLibrariesGuide,version14.7
  PLLE2_BASE_inst: PLLE2_BASE
  generic map(
    BANDWIDTH => "OPTIMIZED", --OPTIMIZED,HIGH,LOW
    CLKFBOUT_MULT  =>    8,   --Multiply value for all CLKOUT, (2-64)
    CLKFBOUT_PHASE =>  0.0,   --Phase offset in degrees of CLKFB,(-360.000-360.000).
    --CLKIN1_PERIOD  => 10.0,   --Input clock period in ns to ps resolution (i.e. 10.0 ns is 100 MHz).
    CLKIN1_PERIOD  =>  8.0,   --Input clock period in ns to ps resolution (i.e.  8.0 ns is 125 MHz).
    -- CLKOUT0_DIVIDE - CLKOUT5_DIVIDE: Divide amount for each CLKOUT (1-128)
    CLKOUT0_DIVIDE =>   CLKOUT0_DIVIDE,   -- CLKIN = 100/125 MHz, CLKOUT = 50 [MHz]
    CLKOUT1_DIVIDE =>   CLKOUT1_DIVIDE,   -- CLKIN = 100/125 MHz, CLKOUT = 10 [MHz]

    CLKOUT2_DIVIDE =>    1,
    CLKOUT3_DIVIDE =>    1,
    CLKOUT4_DIVIDE =>    1,
    CLKOUT5_DIVIDE =>    1,
    -- CLKOUT0_DUTY_CYCLE - CLKOUT5_DUTY_CYCLE: Duty cycle for each CLKOUT (0.001-0.999).
    CLKOUT0_DUTY_CYCLE => 0.5,
    CLKOUT1_DUTY_CYCLE => 0.5,
    CLKOUT2_DUTY_CYCLE => 0.5,
    CLKOUT3_DUTY_CYCLE => 0.5,
    CLKOUT4_DUTY_CYCLE => 0.5,
    CLKOUT5_DUTY_CYCLE => 0.5,
    -- CLKOUT0_PHASE - CLKOUT5_PHASE: Phase offset for each CLKOUT (-360.000 -360.000).
    CLKOUT0_PHASE => 0.0,
    CLKOUT1_PHASE => 0.0,
    CLKOUT2_PHASE => 0.0,
    CLKOUT3_PHASE => 0.0,
    CLKOUT4_PHASE => 0.0,
    CLKOUT5_PHASE => 0.0,
    DIVCLK_DIVIDE =>   1,    --Master division value, (1-56)
    REF_JITTER1   => 0.0,    --Reference input jitter in UI,(0.000-0.999).
    STARTUP_WAIT  =>"TRUE"  --Delay DONE until PLL Locks,("TRUE"/"FALSE")
  )
  port map(
    -- Clock Outputs: 1-bit (each) output: User configurable clock outputs
    CLKOUT0 => system_clk,--1-bit output: CLKOUT0
    CLKOUT1 => timer_clk, --1-bit output: CLKOUT1
    CLKOUT2 => open,      --1-bit output: CLKOUT2
    CLKOUT3 => open,      --1-bit output: CLKOUT3
    CLKOUT4 => open,      --1-bit output: CLKOUT4
    CLKOUT5 => open,      --1-bit output: CLKOUT5
    -- Feedback Clocks: 1-bit (each) output: Clock feedback ports
    CLKFBOUT => sig_clkfb,--1-bit output: Feedback clock
    LOCKED   => locked,   --1-bit output: LOCK
    CLKIN1   => clk,      --1-bit input:  Inputclock
    -- Control Ports:   1-bit (each) input:  PLL control ports
    PWRDWN   => '0',      --1-bit input:  Power-down
    RST      => sig_rst,  --1-bit input:  Reset
    -- Feedback Clocks: 1-bit (each) input: Clock feedback ports 
    CLKFBIN  => sig_clkfb --1-bit input:  Feedback clock
  );
  --End of PLLE2_BASE_inst instantiation

  sig_rst <= not(reset_n);

end Behavioral;
