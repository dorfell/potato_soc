# Potato SoC
This repository is based on the Cristian Skordal Potato processor with a few changes listed below:
* The clock wizard IP core from Vivado was replaced by the clock_generator.vhd file, that uses primitives to employ the DCM  FPGA resources and which can be configured for several clock inputs including 100 [MHz] (e.g. Nexys4DDR, Arty 7), and 125 [MHz] for Zybo Z7.
* The BRAM generator IP core was replaced by an inferred rom description able to read applications .hex files.
* The memory map had a bootrom size of 2 [kB], so it was modified to support up to 16 [kB]. And the *.hex application is being execute from this bootrom.
* Several applications for testing the peripherals were created, these can be found in the `software/` folder, including applications for uart, gpio and timer. Also, a new funcion to sent 32 bits hex data over uart was added to `software/libsoc/uart.h`-> uart_tx_hex.
* The application linker `software/app_folder/app.ld` was modified to be coherent with the updated memory map and also the *.coe files were reemplaced by *.hex files in the `software/Makefile`.
* Simulation, synthesis, implementation, bitstream generation and FPGA programming can be done from the `potato_soc/Makefile`. However, the Vivado GUI can also be used by adding  the `hardware/rtl/` sources to a new project.
* The SoC was tested in a Zybo Z7  development board, however as these SoC employs only the FPGA it is possible to implement the design in others development boards such as the Arty7, Nexys 4, etc. 

The number of resources used for this version of the potato_soc  in a zc07020 FPGA is listed below: 
* LUT: 3172
* LUTRAM 136
* FF: 2271
* BRAM 42
* IO 18
* BUFG 3
* PLL 1

Finally, documentation from the original potato repository can be found further down.

# The Potato Processor

![Processor architecture overview diagramme](https://github.com/skordal/potato/blob/master/docs/diagram.png?raw=true)

The Potato Processor is a simple RISC-V processor written in VHDL for use in FPGAs. It implements the 32-bit integer subset
of the RISC-V Specification version 2.0 and supports large parts of the the machine mode specified in the RISC-V Privileged
Architecture Specification v1.10.

The processor has been tested on an Arty board using the example SoC design provided in the `example/` directory
and the applications found in the `software/` directory. Synthesis and implementation has been tested on various versions
of Xilinx' Vivado toolchain, most recently version 2018.2.

## Features

* Supports the complete 32-bit RISC-V base integer ISA (RV32I) version 2.0
* Supports large parts of the machine mode defined in the RISC-V Privileged Architecture version 1.10
* Supports up to 8 individually maskable external interrupts (IRQs)
* 5-stage "classic" RISC pipeline
* Optional instruction cache
* Supports the Wishbone bus, version B4

## Peripherals

The project includes a variety of Wishbone-compatible peripherals for use in system-on-chip designs based on the Potato processor.
The main peripherals are:

* Timer - a 32-bit timer with compare interrupt
* GPIO - a configurable-width generic GPIO module
* Memory - a block RAM memory module
* UART - a UART module with hardware FIFOs, configurable baudrate and RX/TX interrupts

## Quick Start/Instantiating

To instantiate the processor, add the source files from the `src/` folder to your project. Use the `pp_potato`
entity to instantiate a processor with a Wishbone interface. Some generics are provided to configure the processor core.

An example System-on-Chip for the Arty development board can be found in the `example/` directory of the source repository.

## Compiler Toolchain

To program the processor, you need an appropriate compiler toolchain. Follow the instructions on the
[RISCV GNU toolchain repository](https://github.com/riscv/riscv-gnu-toolchain) site to build and install a 32-bit RISC-V toolchain.

## Reporting bugs and issues

Bugs and issues related to the Potato processor can be reported on the project's [GitHub page](https://github.com/skordal/potato).

